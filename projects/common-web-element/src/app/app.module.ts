import { BrowserModule } from "@angular/platform-browser";
import {
  NgModule,
  DoBootstrap,
  Injector,
  CUSTOM_ELEMENTS_SCHEMA,
} from "@angular/core";

import { AppComponent } from "./app.component";
import {
  HelloModule,
  HelloComponent,
  ButtonComponent,
  ButtonModule,
  FooterComponent,
  FooterModule,
  NavbarComponent,
  NavbarModule,
  HeaderModule,
  HeaderComponent,
} from "projects/common-web-component/src/public-api";

import { createCustomElement } from "@angular/elements";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HelloModule,
    ButtonModule,
    FooterModule,
    NavbarModule,
    HeaderModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    HelloComponent,
    ButtonComponent,
    FooterComponent,
    NavbarComponent,
    HeaderComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule implements DoBootstrap {
  constructor(injector: Injector) {
    const hello = createCustomElement(HelloComponent, { injector });
    const button = createCustomElement(ButtonComponent, { injector });
    const footer = createCustomElement(FooterComponent, { injector });
    const navbar = createCustomElement(NavbarComponent, { injector });
    const header = createCustomElement(HeaderComponent, { injector });
    customElements.define("ele-hello", hello);
    customElements.define("ele-button", button);
    customElements.define("ele-footer", footer);
    customElements.define("ele-navbar", navbar);
    customElements.define("ele-header", header);
  }

  ngDoBootstrap() {}
}
