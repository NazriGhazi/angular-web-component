import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ButtonComponent } from "./button.component";

import { HttpClientModule } from "@angular/common/http";

@NgModule({
  imports: [CommonModule, HttpClientModule],
  declarations: [ButtonComponent],
  providers: [],
  exports: [ButtonComponent],
})
export class ButtonModule {}
