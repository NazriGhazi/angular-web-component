import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HelloComponent } from "./hello.component";

import { HttpClientModule } from "@angular/common/http";

@NgModule({
  imports: [CommonModule, HttpClientModule],
  declarations: [HelloComponent],
  providers: [],
  exports: [HelloComponent],
})
export class HelloModule {}
