import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FooterComponent } from "./footer.component";

import { HttpClientModule } from "@angular/common/http";

@NgModule({
  imports: [CommonModule, HttpClientModule],
  declarations: [FooterComponent],
  providers: [],
  exports: [FooterComponent],
})
export class FooterModule {}
