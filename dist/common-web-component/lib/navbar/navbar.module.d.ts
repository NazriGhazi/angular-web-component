import * as i0 from "@angular/core";
import * as i1 from "./navbar.component";
import * as i2 from "@angular/common";
import * as i3 from "@angular/common/http";
export declare class NavbarModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<NavbarModule, [typeof i1.NavbarComponent], [typeof i2.CommonModule, typeof i3.HttpClientModule], [typeof i1.NavbarComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<NavbarModule>;
}
