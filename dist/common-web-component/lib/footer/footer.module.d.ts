import * as i0 from "@angular/core";
import * as i1 from "./footer.component";
import * as i2 from "@angular/common";
import * as i3 from "@angular/common/http";
export declare class FooterModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<FooterModule, [typeof i1.FooterComponent], [typeof i2.CommonModule, typeof i3.HttpClientModule], [typeof i1.FooterComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<FooterModule>;
}
