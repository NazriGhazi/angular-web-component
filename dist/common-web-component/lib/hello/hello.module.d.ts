import * as i0 from "@angular/core";
import * as i1 from "./hello.component";
import * as i2 from "@angular/common";
import * as i3 from "@angular/common/http";
export declare class HelloModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<HelloModule, [typeof i1.HelloComponent], [typeof i2.CommonModule, typeof i3.HttpClientModule], [typeof i1.HelloComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<HelloModule>;
}
