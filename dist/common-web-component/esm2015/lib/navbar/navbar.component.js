import { Component } from '@angular/core';
import * as i0 from "@angular/core";
export class NavbarComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
NavbarComponent.ɵfac = function NavbarComponent_Factory(t) { return new (t || NavbarComponent)(); };
NavbarComponent.ɵcmp = i0.ɵɵdefineComponent({ type: NavbarComponent, selectors: [["lib-navbar"]], decls: 22, vars: 0, consts: [[1, "navbar", "navbar-expand-lg", "navbar-dark", "bg-dark"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarTogglerDemo03", "aria-controls", "navbarTogglerDemo03", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler"], [1, "navbar-toggler-icon"], ["href", "#", 1, "navbar-brand"], ["id", "navbarTogglerDemo03", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "mr-auto", "mt-2", "mt-lg-0"], [1, "nav-item"], ["href", "#", 1, "nav-link"], [1, "sr-only"], [1, "form-inline", "my-2", "my-lg-0"], ["type", "search", "placeholder", "Search", "aria-label", "Search", 1, "form-control", "mr-sm-2"], ["type", "submit", 1, "btn", "btn-outline-primary", "my-2", "my-sm-0"]], template: function NavbarComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "nav", 0);
        i0.ɵɵelementStart(1, "button", 1);
        i0.ɵɵelement(2, "span", 2);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(3, "a", 3);
        i0.ɵɵtext(4, "Brand");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "div", 4);
        i0.ɵɵelementStart(6, "ul", 5);
        i0.ɵɵelementStart(7, "li", 6);
        i0.ɵɵelementStart(8, "a", 7);
        i0.ɵɵtext(9, "Home ");
        i0.ɵɵelementStart(10, "span", 8);
        i0.ɵɵtext(11, "(current)");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(12, "li", 6);
        i0.ɵɵelementStart(13, "a", 7);
        i0.ɵɵtext(14, "Link");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(15, "li", 6);
        i0.ɵɵelementStart(16, "a", 7);
        i0.ɵɵtext(17, "About");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(18, "form", 9);
        i0.ɵɵelement(19, "input", 10);
        i0.ɵɵelementStart(20, "button", 11);
        i0.ɵɵtext(21, " Search ");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(NavbarComponent, [{
        type: Component,
        args: [{
                selector: 'lib-navbar',
                templateUrl: './navbar.component.html',
                styleUrls: ['./navbar.component.css']
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2YmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi13ZWItY29tcG9uZW50LyIsInNvdXJjZXMiOlsibGliL25hdmJhci9uYXZiYXIuY29tcG9uZW50LnRzIiwibGliL25hdmJhci9uYXZiYXIuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQzs7QUFPbEQsTUFBTSxPQUFPLGVBQWU7SUFFMUI7SUFBZ0IsQ0FBQztJQUVqQixRQUFRO0lBQ1IsQ0FBQzs7OEVBTFUsZUFBZTtvREFBZixlQUFlO1FDUDVCLDhCQUNFO1FBQUEsaUNBU0U7UUFBQSwwQkFBeUM7UUFDM0MsaUJBQVM7UUFDVCw0QkFBaUM7UUFBQSxxQkFBSztRQUFBLGlCQUFJO1FBRTFDLDhCQUNFO1FBQUEsNkJBQ0U7UUFBQSw2QkFDRTtRQUFBLDRCQUNHO1FBQUEscUJBQUs7UUFBQSxnQ0FBc0I7UUFBQSwwQkFBUztRQUFBLGlCQUFPO1FBQUEsaUJBQzdDO1FBQ0gsaUJBQUs7UUFDTCw4QkFDRTtRQUFBLDZCQUE2QjtRQUFBLHFCQUFJO1FBQUEsaUJBQUk7UUFDdkMsaUJBQUs7UUFDTCw4QkFDRTtRQUFBLDZCQUE2QjtRQUFBLHNCQUFLO1FBQUEsaUJBQUk7UUFDeEMsaUJBQUs7UUFDUCxpQkFBSztRQUNMLGdDQUNFO1FBQUEsNkJBTUE7UUFBQSxtQ0FDRTtRQUFBLHlCQUNGO1FBQUEsaUJBQVM7UUFDWCxpQkFBTztRQUNULGlCQUFNO1FBQ1IsaUJBQU07O2tERGpDTyxlQUFlO2NBTDNCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsV0FBVyxFQUFFLHlCQUF5QjtnQkFDdEMsU0FBUyxFQUFFLENBQUMsd0JBQXdCLENBQUM7YUFDdEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItbmF2YmFyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL25hdmJhci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL25hdmJhci5jb21wb25lbnQuY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTmF2YmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICB9XG5cbn1cbiIsIjxuYXYgY2xhc3M9XCJuYXZiYXIgbmF2YmFyLWV4cGFuZC1sZyBuYXZiYXItZGFyayBiZy1kYXJrXCI+XG4gIDxidXR0b25cbiAgICBjbGFzcz1cIm5hdmJhci10b2dnbGVyXCJcbiAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICBkYXRhLXRvZ2dsZT1cImNvbGxhcHNlXCJcbiAgICBkYXRhLXRhcmdldD1cIiNuYXZiYXJUb2dnbGVyRGVtbzAzXCJcbiAgICBhcmlhLWNvbnRyb2xzPVwibmF2YmFyVG9nZ2xlckRlbW8wM1wiXG4gICAgYXJpYS1leHBhbmRlZD1cImZhbHNlXCJcbiAgICBhcmlhLWxhYmVsPVwiVG9nZ2xlIG5hdmlnYXRpb25cIlxuICA+XG4gICAgPHNwYW4gY2xhc3M9XCJuYXZiYXItdG9nZ2xlci1pY29uXCI+PC9zcGFuPlxuICA8L2J1dHRvbj5cbiAgPGEgY2xhc3M9XCJuYXZiYXItYnJhbmRcIiBocmVmPVwiI1wiPkJyYW5kPC9hPlxuXG4gIDxkaXYgY2xhc3M9XCJjb2xsYXBzZSBuYXZiYXItY29sbGFwc2VcIiBpZD1cIm5hdmJhclRvZ2dsZXJEZW1vMDNcIj5cbiAgICA8dWwgY2xhc3M9XCJuYXZiYXItbmF2IG1yLWF1dG8gbXQtMiBtdC1sZy0wXCI+XG4gICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxuICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIiNcIlxuICAgICAgICAgID5Ib21lIDxzcGFuIGNsYXNzPVwic3Itb25seVwiPihjdXJyZW50KTwvc3Bhbj48L2FcbiAgICAgICAgPlxuICAgICAgPC9saT5cbiAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XG4gICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiI1wiPkxpbms8L2E+XG4gICAgICA8L2xpPlxuICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cbiAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIjXCI+QWJvdXQ8L2E+XG4gICAgICA8L2xpPlxuICAgIDwvdWw+XG4gICAgPGZvcm0gY2xhc3M9XCJmb3JtLWlubGluZSBteS0yIG15LWxnLTBcIj5cbiAgICAgIDxpbnB1dFxuICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbCBtci1zbS0yXCJcbiAgICAgICAgdHlwZT1cInNlYXJjaFwiXG4gICAgICAgIHBsYWNlaG9sZGVyPVwiU2VhcmNoXCJcbiAgICAgICAgYXJpYS1sYWJlbD1cIlNlYXJjaFwiXG4gICAgICAvPlxuICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tb3V0bGluZS1wcmltYXJ5IG15LTIgbXktc20tMFwiIHR5cGU9XCJzdWJtaXRcIj5cbiAgICAgICAgU2VhcmNoXG4gICAgICA8L2J1dHRvbj5cbiAgICA8L2Zvcm0+XG4gIDwvZGl2PlxuPC9uYXY+XG4iXX0=