import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NavbarComponent } from "./navbar.component";
import { HttpClientModule } from "@angular/common/http";
import * as i0 from "@angular/core";
export class NavbarModule {
}
NavbarModule.ɵmod = i0.ɵɵdefineNgModule({ type: NavbarModule });
NavbarModule.ɵinj = i0.ɵɵdefineInjector({ factory: function NavbarModule_Factory(t) { return new (t || NavbarModule)(); }, providers: [], imports: [[CommonModule, HttpClientModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(NavbarModule, { declarations: [NavbarComponent], imports: [CommonModule, HttpClientModule], exports: [NavbarComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(NavbarModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule, HttpClientModule],
                declarations: [NavbarComponent],
                providers: [],
                exports: [NavbarComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2YmFyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi13ZWItY29tcG9uZW50LyIsInNvdXJjZXMiOlsibGliL25hdmJhci9uYXZiYXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVyRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7QUFReEQsTUFBTSxPQUFPLFlBQVk7O2dEQUFaLFlBQVk7dUdBQVosWUFBWSxtQkFIWixFQUFFLFlBRkosQ0FBQyxZQUFZLEVBQUUsZ0JBQWdCLENBQUM7d0ZBSzlCLFlBQVksbUJBSlIsZUFBZSxhQURwQixZQUFZLEVBQUUsZ0JBQWdCLGFBRzlCLGVBQWU7a0RBRWQsWUFBWTtjQU54QixRQUFRO2VBQUM7Z0JBQ1IsT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLGdCQUFnQixDQUFDO2dCQUN6QyxZQUFZLEVBQUUsQ0FBQyxlQUFlLENBQUM7Z0JBQy9CLFNBQVMsRUFBRSxFQUFFO2dCQUNiLE9BQU8sRUFBRSxDQUFDLGVBQWUsQ0FBQzthQUMzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcblxuaW1wb3J0IHsgTmF2YmFyQ29tcG9uZW50IH0gZnJvbSBcIi4vbmF2YmFyLmNvbXBvbmVudFwiO1xuXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsIEh0dHBDbGllbnRNb2R1bGVdLFxuICBkZWNsYXJhdGlvbnM6IFtOYXZiYXJDb21wb25lbnRdLFxuICBwcm92aWRlcnM6IFtdLFxuICBleHBvcnRzOiBbTmF2YmFyQ29tcG9uZW50XSxcbn0pXG5leHBvcnQgY2xhc3MgTmF2YmFyTW9kdWxlIHt9XG4iXX0=