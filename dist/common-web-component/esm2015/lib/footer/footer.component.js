import { Component } from '@angular/core';
import * as i0 from "@angular/core";
export class FooterComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
FooterComponent.ɵfac = function FooterComponent_Factory(t) { return new (t || FooterComponent)(); };
FooterComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FooterComponent, selectors: [["lib-footer"]], decls: 76, vars: 0, consts: [[1, "page-footer", "font-small", "bg-dark", "text-white"], [2, "background-color", "#6351ce"], [1, "container"], [1, "row", "py-4", "d-flex", "align-items-center"], [1, "col-md-6", "col-lg-5", "text-center", "text-md-left", "mb-4", "mb-md-0"], [1, "mb-0"], [1, "col-md-6", "col-lg-7", "text-center", "text-md-right"], [1, "fb-ic"], [1, "fab", "fa-facebook-f", "white-text", "mr-4"], [1, "tw-ic"], [1, "fab", "fa-twitter", "white-text", "mr-4"], [1, "gplus-ic"], [1, "fab", "fa-google-plus-g", "white-text", "mr-4"], [1, "li-ic"], [1, "fab", "fa-linkedin-in", "white-text", "mr-4"], [1, "ins-ic"], [1, "fab", "fa-instagram", "white-text"], [1, "container", "text-center", "text-md-left", "mt-5"], [1, "row", "mt-3"], [1, "col-md-3", "col-lg-4", "col-xl-3", "mx-auto", "mb-4"], [1, "text-uppercase", "font-weight-bold"], [1, "deep-purple", "accent-2", "mb-4", "mt-0", "d-inline-block", "mx-auto", 2, "width", "60px"], [1, "col-md-2", "col-lg-2", "col-xl-2", "mx-auto", "mb-4"], ["href", "#!"], [1, "col-md-3", "col-lg-2", "col-xl-2", "mx-auto", "mb-4"], [1, "col-md-4", "col-lg-3", "col-xl-3", "mx-auto", "mb-md-0", "mb-4"], [1, "fas", "fa-home", "mr-3"], [1, "fas", "fa-envelope", "mr-3"], [1, "fas", "fa-phone", "mr-3"], [1, "fas", "fa-print", "mr-3"], [1, "footer-copyright", "text-center", "py-3"]], template: function FooterComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "footer", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵelementStart(2, "div", 2);
        i0.ɵɵelementStart(3, "div", 3);
        i0.ɵɵelementStart(4, "div", 4);
        i0.ɵɵelementStart(5, "h6", 5);
        i0.ɵɵtext(6, "Get connected with us on social networks!");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(7, "div", 6);
        i0.ɵɵelementStart(8, "a", 7);
        i0.ɵɵelement(9, "i", 8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(10, "a", 9);
        i0.ɵɵelement(11, "i", 10);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(12, "a", 11);
        i0.ɵɵelement(13, "i", 12);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(14, "a", 13);
        i0.ɵɵelement(15, "i", 14);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(16, "a", 15);
        i0.ɵɵelement(17, "i", 16);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(18, "div", 17);
        i0.ɵɵelementStart(19, "div", 18);
        i0.ɵɵelementStart(20, "div", 19);
        i0.ɵɵelementStart(21, "h6", 20);
        i0.ɵɵtext(22, "Company name");
        i0.ɵɵelementEnd();
        i0.ɵɵelement(23, "hr", 21);
        i0.ɵɵelementStart(24, "p");
        i0.ɵɵtext(25, " Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet, consectetur adipisicing elit. ");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(26, "div", 22);
        i0.ɵɵelementStart(27, "h6", 20);
        i0.ɵɵtext(28, "Products");
        i0.ɵɵelementEnd();
        i0.ɵɵelement(29, "hr", 21);
        i0.ɵɵelementStart(30, "p");
        i0.ɵɵelementStart(31, "a", 23);
        i0.ɵɵtext(32, "MDBootstrap");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(33, "p");
        i0.ɵɵelementStart(34, "a", 23);
        i0.ɵɵtext(35, "MDWordPress");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(36, "p");
        i0.ɵɵelementStart(37, "a", 23);
        i0.ɵɵtext(38, "BrandFlow");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(39, "p");
        i0.ɵɵelementStart(40, "a", 23);
        i0.ɵɵtext(41, "Bootstrap Angular");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(42, "div", 24);
        i0.ɵɵelementStart(43, "h6", 20);
        i0.ɵɵtext(44, "Useful links");
        i0.ɵɵelementEnd();
        i0.ɵɵelement(45, "hr", 21);
        i0.ɵɵelementStart(46, "p");
        i0.ɵɵelementStart(47, "a", 23);
        i0.ɵɵtext(48, "Your Account");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(49, "p");
        i0.ɵɵelementStart(50, "a", 23);
        i0.ɵɵtext(51, "Become an Affiliate");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(52, "p");
        i0.ɵɵelementStart(53, "a", 23);
        i0.ɵɵtext(54, "Shipping Rates");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(55, "p");
        i0.ɵɵelementStart(56, "a", 23);
        i0.ɵɵtext(57, "Help");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(58, "div", 25);
        i0.ɵɵelementStart(59, "h6", 20);
        i0.ɵɵtext(60, "Contact");
        i0.ɵɵelementEnd();
        i0.ɵɵelement(61, "hr", 21);
        i0.ɵɵelementStart(62, "p");
        i0.ɵɵelement(63, "i", 26);
        i0.ɵɵtext(64, " New York, NY 10012, US");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(65, "p");
        i0.ɵɵelement(66, "i", 27);
        i0.ɵɵtext(67, " info@example.com");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(68, "p");
        i0.ɵɵelement(69, "i", 28);
        i0.ɵɵtext(70, " + 01 234 567 88");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(71, "p");
        i0.ɵɵelement(72, "i", 29);
        i0.ɵɵtext(73, " + 01 234 567 89");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(74, "div", 30);
        i0.ɵɵtext(75, " \u00A9 2020 Copyright: ");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FooterComponent, [{
        type: Component,
        args: [{
                selector: 'lib-footer',
                templateUrl: './footer.component.html',
                styleUrls: ['./footer.component.css']
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi13ZWItY29tcG9uZW50LyIsInNvdXJjZXMiOlsibGliL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnRzIiwibGliL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQzs7QUFPbEQsTUFBTSxPQUFPLGVBQWU7SUFFMUI7SUFBZ0IsQ0FBQztJQUVqQixRQUFRO0lBQ1IsQ0FBQzs7OEVBTFUsZUFBZTtvREFBZixlQUFlO1FDTjVCLGlDQUNFO1FBQUEsOEJBQ0U7UUFBQSw4QkFDRTtRQUNBLDhCQUNFO1FBQ0EsOEJBQ0U7UUFBQSw2QkFBaUI7UUFBQSx5REFBeUM7UUFBQSxpQkFBSztRQUNqRSxpQkFBTTtRQUlOLDhCQUNFO1FBQ0EsNEJBQ0U7UUFBQSx1QkFBa0Q7UUFDcEQsaUJBQUk7UUFFSiw2QkFDRTtRQUFBLHlCQUErQztRQUNqRCxpQkFBSTtRQUVKLDhCQUNFO1FBQUEseUJBQXFEO1FBQ3ZELGlCQUFJO1FBRUosOEJBQ0U7UUFBQSx5QkFBbUQ7UUFDckQsaUJBQUk7UUFFSiw4QkFDRTtRQUFBLHlCQUE0QztRQUM5QyxpQkFBSTtRQUNOLGlCQUFNO1FBRVIsaUJBQU07UUFFUixpQkFBTTtRQUNSLGlCQUFNO1FBR04sZ0NBQ0U7UUFDQSxnQ0FDRTtRQUNBLGdDQUNFO1FBQ0EsK0JBQTRDO1FBQUEsNkJBQVk7UUFBQSxpQkFBSztRQUM3RCwwQkFJQTtRQUFBLDBCQUNFO1FBQUEsK0lBRUY7UUFBQSxpQkFBSTtRQUNOLGlCQUFNO1FBSU4sZ0NBQ0U7UUFDQSwrQkFBNEM7UUFBQSx5QkFBUTtRQUFBLGlCQUFLO1FBQ3pELDBCQUlBO1FBQUEsMEJBQ0U7UUFBQSw4QkFBYTtRQUFBLDRCQUFXO1FBQUEsaUJBQUk7UUFDOUIsaUJBQUk7UUFDSiwwQkFDRTtRQUFBLDhCQUFhO1FBQUEsNEJBQVc7UUFBQSxpQkFBSTtRQUM5QixpQkFBSTtRQUNKLDBCQUNFO1FBQUEsOEJBQWE7UUFBQSwwQkFBUztRQUFBLGlCQUFJO1FBQzVCLGlCQUFJO1FBQ0osMEJBQ0U7UUFBQSw4QkFBYTtRQUFBLGtDQUFpQjtRQUFBLGlCQUFJO1FBQ3BDLGlCQUFJO1FBQ04saUJBQU07UUFJTixnQ0FDRTtRQUNBLCtCQUE0QztRQUFBLDZCQUFZO1FBQUEsaUJBQUs7UUFDN0QsMEJBSUE7UUFBQSwwQkFDRTtRQUFBLDhCQUFhO1FBQUEsNkJBQVk7UUFBQSxpQkFBSTtRQUMvQixpQkFBSTtRQUNKLDBCQUNFO1FBQUEsOEJBQWE7UUFBQSxvQ0FBbUI7UUFBQSxpQkFBSTtRQUN0QyxpQkFBSTtRQUNKLDBCQUNFO1FBQUEsOEJBQWE7UUFBQSwrQkFBYztRQUFBLGlCQUFJO1FBQ2pDLGlCQUFJO1FBQ0osMEJBQ0U7UUFBQSw4QkFBYTtRQUFBLHFCQUFJO1FBQUEsaUJBQUk7UUFDdkIsaUJBQUk7UUFDTixpQkFBTTtRQUlOLGdDQUNFO1FBQ0EsK0JBQTRDO1FBQUEsd0JBQU87UUFBQSxpQkFBSztRQUN4RCwwQkFJQTtRQUFBLDBCQUFHO1FBQUEseUJBQWdDO1FBQUMsd0NBQXNCO1FBQUEsaUJBQUk7UUFDOUQsMEJBQUc7UUFBQSx5QkFBb0M7UUFBQyxrQ0FBZ0I7UUFBQSxpQkFBSTtRQUM1RCwwQkFBRztRQUFBLHlCQUFpQztRQUFDLGlDQUFlO1FBQUEsaUJBQUk7UUFDeEQsMEJBQUc7UUFBQSx5QkFBaUM7UUFBQyxpQ0FBZTtRQUFBLGlCQUFJO1FBQzFELGlCQUFNO1FBRVIsaUJBQU07UUFFUixpQkFBTTtRQUlOLGdDQUNFO1FBQUEseUNBQ0Y7UUFBQSxpQkFBTTtRQUVSLGlCQUFTOztrREQzSEksZUFBZTtjQUwzQixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLFdBQVcsRUFBRSx5QkFBeUI7Z0JBQ3RDLFNBQVMsRUFBRSxDQUFDLHdCQUF3QixDQUFDO2FBQ3RDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLWZvb3RlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9mb290ZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9mb290ZXIuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIEZvb3RlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgfVxuXG59XG4iLCI8IS0tIEZvb3RlciAtLT5cbjxmb290ZXIgY2xhc3M9XCJwYWdlLWZvb3RlciBmb250LXNtYWxsIGJnLWRhcmsgdGV4dC13aGl0ZVwiPlxuICA8ZGl2IHN0eWxlPVwiYmFja2dyb3VuZC1jb2xvcjogIzYzNTFjZTtcIj5cbiAgICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyXCI+XG4gICAgICA8IS0tIEdyaWQgcm93LS0+XG4gICAgICA8ZGl2IGNsYXNzPVwicm93IHB5LTQgZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlclwiPlxuICAgICAgICA8IS0tIEdyaWQgY29sdW1uIC0tPlxuICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTYgY29sLWxnLTUgdGV4dC1jZW50ZXIgdGV4dC1tZC1sZWZ0IG1iLTQgbWItbWQtMFwiPlxuICAgICAgICAgIDxoNiBjbGFzcz1cIm1iLTBcIj5HZXQgY29ubmVjdGVkIHdpdGggdXMgb24gc29jaWFsIG5ldHdvcmtzITwvaDY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8IS0tIEdyaWQgY29sdW1uIC0tPlxuXG4gICAgICAgIDwhLS0gR3JpZCBjb2x1bW4gLS0+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNiBjb2wtbGctNyB0ZXh0LWNlbnRlciB0ZXh0LW1kLXJpZ2h0XCI+XG4gICAgICAgICAgPCEtLSBGYWNlYm9vayAtLT5cbiAgICAgICAgICA8YSBjbGFzcz1cImZiLWljXCI+XG4gICAgICAgICAgICA8aSBjbGFzcz1cImZhYiBmYS1mYWNlYm9vay1mIHdoaXRlLXRleHQgbXItNFwiPiA8L2k+XG4gICAgICAgICAgPC9hPlxuICAgICAgICAgIDwhLS0gVHdpdHRlciAtLT5cbiAgICAgICAgICA8YSBjbGFzcz1cInR3LWljXCI+XG4gICAgICAgICAgICA8aSBjbGFzcz1cImZhYiBmYS10d2l0dGVyIHdoaXRlLXRleHQgbXItNFwiPiA8L2k+XG4gICAgICAgICAgPC9hPlxuICAgICAgICAgIDwhLS0gR29vZ2xlICstLT5cbiAgICAgICAgICA8YSBjbGFzcz1cImdwbHVzLWljXCI+XG4gICAgICAgICAgICA8aSBjbGFzcz1cImZhYiBmYS1nb29nbGUtcGx1cy1nIHdoaXRlLXRleHQgbXItNFwiPiA8L2k+XG4gICAgICAgICAgPC9hPlxuICAgICAgICAgIDwhLS1MaW5rZWRpbiAtLT5cbiAgICAgICAgICA8YSBjbGFzcz1cImxpLWljXCI+XG4gICAgICAgICAgICA8aSBjbGFzcz1cImZhYiBmYS1saW5rZWRpbi1pbiB3aGl0ZS10ZXh0IG1yLTRcIj4gPC9pPlxuICAgICAgICAgIDwvYT5cbiAgICAgICAgICA8IS0tSW5zdGFncmFtLS0+XG4gICAgICAgICAgPGEgY2xhc3M9XCJpbnMtaWNcIj5cbiAgICAgICAgICAgIDxpIGNsYXNzPVwiZmFiIGZhLWluc3RhZ3JhbSB3aGl0ZS10ZXh0XCI+IDwvaT5cbiAgICAgICAgICA8L2E+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8IS0tIEdyaWQgY29sdW1uIC0tPlxuICAgICAgPC9kaXY+XG4gICAgICA8IS0tIEdyaWQgcm93LS0+XG4gICAgPC9kaXY+XG4gIDwvZGl2PlxuXG4gIDwhLS0gRm9vdGVyIExpbmtzIC0tPlxuICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyIHRleHQtY2VudGVyIHRleHQtbWQtbGVmdCBtdC01XCI+XG4gICAgPCEtLSBHcmlkIHJvdyAtLT5cbiAgICA8ZGl2IGNsYXNzPVwicm93IG10LTNcIj5cbiAgICAgIDwhLS0gR3JpZCBjb2x1bW4gLS0+XG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTMgY29sLWxnLTQgY29sLXhsLTMgbXgtYXV0byBtYi00XCI+XG4gICAgICAgIDwhLS0gQ29udGVudCAtLT5cbiAgICAgICAgPGg2IGNsYXNzPVwidGV4dC11cHBlcmNhc2UgZm9udC13ZWlnaHQtYm9sZFwiPkNvbXBhbnkgbmFtZTwvaDY+XG4gICAgICAgIDxoclxuICAgICAgICAgIGNsYXNzPVwiZGVlcC1wdXJwbGUgYWNjZW50LTIgbWItNCBtdC0wIGQtaW5saW5lLWJsb2NrIG14LWF1dG9cIlxuICAgICAgICAgIHN0eWxlPVwid2lkdGg6IDYwcHg7XCJcbiAgICAgICAgLz5cbiAgICAgICAgPHA+XG4gICAgICAgICAgSGVyZSB5b3UgY2FuIHVzZSByb3dzIGFuZCBjb2x1bW5zIHRvIG9yZ2FuaXplIHlvdXIgZm9vdGVyIGNvbnRlbnQuXG4gICAgICAgICAgTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuXG4gICAgICAgIDwvcD5cbiAgICAgIDwvZGl2PlxuICAgICAgPCEtLSBHcmlkIGNvbHVtbiAtLT5cblxuICAgICAgPCEtLSBHcmlkIGNvbHVtbiAtLT5cbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMiBjb2wtbGctMiBjb2wteGwtMiBteC1hdXRvIG1iLTRcIj5cbiAgICAgICAgPCEtLSBMaW5rcyAtLT5cbiAgICAgICAgPGg2IGNsYXNzPVwidGV4dC11cHBlcmNhc2UgZm9udC13ZWlnaHQtYm9sZFwiPlByb2R1Y3RzPC9oNj5cbiAgICAgICAgPGhyXG4gICAgICAgICAgY2xhc3M9XCJkZWVwLXB1cnBsZSBhY2NlbnQtMiBtYi00IG10LTAgZC1pbmxpbmUtYmxvY2sgbXgtYXV0b1wiXG4gICAgICAgICAgc3R5bGU9XCJ3aWR0aDogNjBweDtcIlxuICAgICAgICAvPlxuICAgICAgICA8cD5cbiAgICAgICAgICA8YSBocmVmPVwiIyFcIj5NREJvb3RzdHJhcDwvYT5cbiAgICAgICAgPC9wPlxuICAgICAgICA8cD5cbiAgICAgICAgICA8YSBocmVmPVwiIyFcIj5NRFdvcmRQcmVzczwvYT5cbiAgICAgICAgPC9wPlxuICAgICAgICA8cD5cbiAgICAgICAgICA8YSBocmVmPVwiIyFcIj5CcmFuZEZsb3c8L2E+XG4gICAgICAgIDwvcD5cbiAgICAgICAgPHA+XG4gICAgICAgICAgPGEgaHJlZj1cIiMhXCI+Qm9vdHN0cmFwIEFuZ3VsYXI8L2E+XG4gICAgICAgIDwvcD5cbiAgICAgIDwvZGl2PlxuICAgICAgPCEtLSBHcmlkIGNvbHVtbiAtLT5cblxuICAgICAgPCEtLSBHcmlkIGNvbHVtbiAtLT5cbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMyBjb2wtbGctMiBjb2wteGwtMiBteC1hdXRvIG1iLTRcIj5cbiAgICAgICAgPCEtLSBMaW5rcyAtLT5cbiAgICAgICAgPGg2IGNsYXNzPVwidGV4dC11cHBlcmNhc2UgZm9udC13ZWlnaHQtYm9sZFwiPlVzZWZ1bCBsaW5rczwvaDY+XG4gICAgICAgIDxoclxuICAgICAgICAgIGNsYXNzPVwiZGVlcC1wdXJwbGUgYWNjZW50LTIgbWItNCBtdC0wIGQtaW5saW5lLWJsb2NrIG14LWF1dG9cIlxuICAgICAgICAgIHN0eWxlPVwid2lkdGg6IDYwcHg7XCJcbiAgICAgICAgLz5cbiAgICAgICAgPHA+XG4gICAgICAgICAgPGEgaHJlZj1cIiMhXCI+WW91ciBBY2NvdW50PC9hPlxuICAgICAgICA8L3A+XG4gICAgICAgIDxwPlxuICAgICAgICAgIDxhIGhyZWY9XCIjIVwiPkJlY29tZSBhbiBBZmZpbGlhdGU8L2E+XG4gICAgICAgIDwvcD5cbiAgICAgICAgPHA+XG4gICAgICAgICAgPGEgaHJlZj1cIiMhXCI+U2hpcHBpbmcgUmF0ZXM8L2E+XG4gICAgICAgIDwvcD5cbiAgICAgICAgPHA+XG4gICAgICAgICAgPGEgaHJlZj1cIiMhXCI+SGVscDwvYT5cbiAgICAgICAgPC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8IS0tIEdyaWQgY29sdW1uIC0tPlxuXG4gICAgICA8IS0tIEdyaWQgY29sdW1uIC0tPlxuICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC00IGNvbC1sZy0zIGNvbC14bC0zIG14LWF1dG8gbWItbWQtMCBtYi00XCI+XG4gICAgICAgIDwhLS0gTGlua3MgLS0+XG4gICAgICAgIDxoNiBjbGFzcz1cInRleHQtdXBwZXJjYXNlIGZvbnQtd2VpZ2h0LWJvbGRcIj5Db250YWN0PC9oNj5cbiAgICAgICAgPGhyXG4gICAgICAgICAgY2xhc3M9XCJkZWVwLXB1cnBsZSBhY2NlbnQtMiBtYi00IG10LTAgZC1pbmxpbmUtYmxvY2sgbXgtYXV0b1wiXG4gICAgICAgICAgc3R5bGU9XCJ3aWR0aDogNjBweDtcIlxuICAgICAgICAvPlxuICAgICAgICA8cD48aSBjbGFzcz1cImZhcyBmYS1ob21lIG1yLTNcIj48L2k+IE5ldyBZb3JrLCBOWSAxMDAxMiwgVVM8L3A+XG4gICAgICAgIDxwPjxpIGNsYXNzPVwiZmFzIGZhLWVudmVsb3BlIG1yLTNcIj48L2k+IGluZm9AZXhhbXBsZS5jb208L3A+XG4gICAgICAgIDxwPjxpIGNsYXNzPVwiZmFzIGZhLXBob25lIG1yLTNcIj48L2k+ICsgMDEgMjM0IDU2NyA4ODwvcD5cbiAgICAgICAgPHA+PGkgY2xhc3M9XCJmYXMgZmEtcHJpbnQgbXItM1wiPjwvaT4gKyAwMSAyMzQgNTY3IDg5PC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8IS0tIEdyaWQgY29sdW1uIC0tPlxuICAgIDwvZGl2PlxuICAgIDwhLS0gR3JpZCByb3cgLS0+XG4gIDwvZGl2PlxuICA8IS0tIEZvb3RlciBMaW5rcyAtLT5cblxuICA8IS0tIENvcHlyaWdodCAtLT5cbiAgPGRpdiBjbGFzcz1cImZvb3Rlci1jb3B5cmlnaHQgdGV4dC1jZW50ZXIgcHktM1wiPlxuICAgIMKpIDIwMjAgQ29weXJpZ2h0OlxuICA8L2Rpdj5cbiAgPCEtLSBDb3B5cmlnaHQgLS0+XG48L2Zvb3Rlcj5cbjwhLS0gRm9vdGVyIC0tPlxuIl19