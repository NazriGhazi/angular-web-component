import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HelloComponent } from "./hello.component";
import { HttpClientModule } from "@angular/common/http";
import * as i0 from "@angular/core";
export class HelloModule {
}
HelloModule.ɵmod = i0.ɵɵdefineNgModule({ type: HelloModule });
HelloModule.ɵinj = i0.ɵɵdefineInjector({ factory: function HelloModule_Factory(t) { return new (t || HelloModule)(); }, providers: [], imports: [[CommonModule, HttpClientModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(HelloModule, { declarations: [HelloComponent], imports: [CommonModule, HttpClientModule], exports: [HelloComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(HelloModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule, HttpClientModule],
                declarations: [HelloComponent],
                providers: [],
                exports: [HelloComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVsbG8ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXdlYi1jb21wb25lbnQvIiwic291cmNlcyI6WyJsaWIvaGVsbG8vaGVsbG8ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUVuRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7QUFReEQsTUFBTSxPQUFPLFdBQVc7OytDQUFYLFdBQVc7cUdBQVgsV0FBVyxtQkFIWCxFQUFFLFlBRkosQ0FBQyxZQUFZLEVBQUUsZ0JBQWdCLENBQUM7d0ZBSzlCLFdBQVcsbUJBSlAsY0FBYyxhQURuQixZQUFZLEVBQUUsZ0JBQWdCLGFBRzlCLGNBQWM7a0RBRWIsV0FBVztjQU52QixRQUFRO2VBQUM7Z0JBQ1IsT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLGdCQUFnQixDQUFDO2dCQUN6QyxZQUFZLEVBQUUsQ0FBQyxjQUFjLENBQUM7Z0JBQzlCLFNBQVMsRUFBRSxFQUFFO2dCQUNiLE9BQU8sRUFBRSxDQUFDLGNBQWMsQ0FBQzthQUMxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5cclxuaW1wb3J0IHsgSGVsbG9Db21wb25lbnQgfSBmcm9tIFwiLi9oZWxsby5jb21wb25lbnRcIjtcclxuXHJcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgSHR0cENsaWVudE1vZHVsZV0sXHJcbiAgZGVjbGFyYXRpb25zOiBbSGVsbG9Db21wb25lbnRdLFxyXG4gIHByb3ZpZGVyczogW10sXHJcbiAgZXhwb3J0czogW0hlbGxvQ29tcG9uZW50XSxcclxufSlcclxuZXhwb3J0IGNsYXNzIEhlbGxvTW9kdWxlIHt9XHJcbiJdfQ==