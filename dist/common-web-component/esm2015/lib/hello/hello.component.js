import { Component } from '@angular/core';
import * as i0 from "@angular/core";
export class HelloComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
HelloComponent.ɵfac = function HelloComponent_Factory(t) { return new (t || HelloComponent)(); };
HelloComponent.ɵcmp = i0.ɵɵdefineComponent({ type: HelloComponent, selectors: [["lib-hello"]], decls: 2, vars: 0, template: function HelloComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "p");
        i0.ɵɵtext(1, "This is from hello component");
        i0.ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(HelloComponent, [{
        type: Component,
        args: [{
                selector: 'lib-hello',
                templateUrl: './hello.component.html',
                styleUrls: ['./hello.component.css']
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVsbG8uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXdlYi1jb21wb25lbnQvIiwic291cmNlcyI6WyJsaWIvaGVsbG8vaGVsbG8uY29tcG9uZW50LnRzIiwibGliL2hlbGxvL2hlbGxvLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7O0FBT2xELE1BQU0sT0FBTyxjQUFjO0lBRXpCO0lBQWdCLENBQUM7SUFFakIsUUFBUTtJQUNSLENBQUM7OzRFQUxVLGNBQWM7bURBQWQsY0FBYztRQ1AzQix5QkFBRztRQUFBLDRDQUE0QjtRQUFBLGlCQUFJOztrRERPdEIsY0FBYztjQUwxQixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLFdBQVcsRUFBRSx3QkFBd0I7Z0JBQ3JDLFNBQVMsRUFBRSxDQUFDLHVCQUF1QixDQUFDO2FBQ3JDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLWhlbGxvJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2hlbGxvLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vaGVsbG8uY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIEhlbGxvQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICB9XG5cbn1cbiIsIjxwPlRoaXMgaXMgZnJvbSBoZWxsbyBjb21wb25lbnQ8L3A+XG4iXX0=