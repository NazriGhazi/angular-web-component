import { ɵɵdefineComponent, ɵɵelementStart, ɵɵtext, ɵɵelementEnd, ɵsetClassMetadata, Component, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule, ɵɵelement } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

class ButtonComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
ButtonComponent.ɵfac = function ButtonComponent_Factory(t) { return new (t || ButtonComponent)(); };
ButtonComponent.ɵcmp = ɵɵdefineComponent({ type: ButtonComponent, selectors: [["lib-button"]], decls: 2, vars: 0, template: function ButtonComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "button");
        ɵɵtext(1, "Button Works");
        ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(ButtonComponent, [{
        type: Component,
        args: [{
                selector: 'lib-button',
                templateUrl: './button.component.html',
                styleUrls: ['./button.component.css']
            }]
    }], function () { return []; }, null); })();

class ButtonModule {
}
ButtonModule.ɵmod = ɵɵdefineNgModule({ type: ButtonModule });
ButtonModule.ɵinj = ɵɵdefineInjector({ factory: function ButtonModule_Factory(t) { return new (t || ButtonModule)(); }, providers: [], imports: [[CommonModule, HttpClientModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(ButtonModule, { declarations: [ButtonComponent], imports: [CommonModule, HttpClientModule], exports: [ButtonComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(ButtonModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule, HttpClientModule],
                declarations: [ButtonComponent],
                providers: [],
                exports: [ButtonComponent],
            }]
    }], null, null); })();

class HelloComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
HelloComponent.ɵfac = function HelloComponent_Factory(t) { return new (t || HelloComponent)(); };
HelloComponent.ɵcmp = ɵɵdefineComponent({ type: HelloComponent, selectors: [["lib-hello"]], decls: 2, vars: 0, template: function HelloComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "p");
        ɵɵtext(1, "This is from hello component");
        ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(HelloComponent, [{
        type: Component,
        args: [{
                selector: 'lib-hello',
                templateUrl: './hello.component.html',
                styleUrls: ['./hello.component.css']
            }]
    }], function () { return []; }, null); })();

class HelloModule {
}
HelloModule.ɵmod = ɵɵdefineNgModule({ type: HelloModule });
HelloModule.ɵinj = ɵɵdefineInjector({ factory: function HelloModule_Factory(t) { return new (t || HelloModule)(); }, providers: [], imports: [[CommonModule, HttpClientModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(HelloModule, { declarations: [HelloComponent], imports: [CommonModule, HttpClientModule], exports: [HelloComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(HelloModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule, HttpClientModule],
                declarations: [HelloComponent],
                providers: [],
                exports: [HelloComponent],
            }]
    }], null, null); })();

class FooterComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
FooterComponent.ɵfac = function FooterComponent_Factory(t) { return new (t || FooterComponent)(); };
FooterComponent.ɵcmp = ɵɵdefineComponent({ type: FooterComponent, selectors: [["lib-footer"]], decls: 76, vars: 0, consts: [[1, "page-footer", "font-small", "bg-dark", "text-white"], [2, "background-color", "#6351ce"], [1, "container"], [1, "row", "py-4", "d-flex", "align-items-center"], [1, "col-md-6", "col-lg-5", "text-center", "text-md-left", "mb-4", "mb-md-0"], [1, "mb-0"], [1, "col-md-6", "col-lg-7", "text-center", "text-md-right"], [1, "fb-ic"], [1, "fab", "fa-facebook-f", "white-text", "mr-4"], [1, "tw-ic"], [1, "fab", "fa-twitter", "white-text", "mr-4"], [1, "gplus-ic"], [1, "fab", "fa-google-plus-g", "white-text", "mr-4"], [1, "li-ic"], [1, "fab", "fa-linkedin-in", "white-text", "mr-4"], [1, "ins-ic"], [1, "fab", "fa-instagram", "white-text"], [1, "container", "text-center", "text-md-left", "mt-5"], [1, "row", "mt-3"], [1, "col-md-3", "col-lg-4", "col-xl-3", "mx-auto", "mb-4"], [1, "text-uppercase", "font-weight-bold"], [1, "deep-purple", "accent-2", "mb-4", "mt-0", "d-inline-block", "mx-auto", 2, "width", "60px"], [1, "col-md-2", "col-lg-2", "col-xl-2", "mx-auto", "mb-4"], ["href", "#!"], [1, "col-md-3", "col-lg-2", "col-xl-2", "mx-auto", "mb-4"], [1, "col-md-4", "col-lg-3", "col-xl-3", "mx-auto", "mb-md-0", "mb-4"], [1, "fas", "fa-home", "mr-3"], [1, "fas", "fa-envelope", "mr-3"], [1, "fas", "fa-phone", "mr-3"], [1, "fas", "fa-print", "mr-3"], [1, "footer-copyright", "text-center", "py-3"]], template: function FooterComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "footer", 0);
        ɵɵelementStart(1, "div", 1);
        ɵɵelementStart(2, "div", 2);
        ɵɵelementStart(3, "div", 3);
        ɵɵelementStart(4, "div", 4);
        ɵɵelementStart(5, "h6", 5);
        ɵɵtext(6, "Get connected with us on social networks!");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(7, "div", 6);
        ɵɵelementStart(8, "a", 7);
        ɵɵelement(9, "i", 8);
        ɵɵelementEnd();
        ɵɵelementStart(10, "a", 9);
        ɵɵelement(11, "i", 10);
        ɵɵelementEnd();
        ɵɵelementStart(12, "a", 11);
        ɵɵelement(13, "i", 12);
        ɵɵelementEnd();
        ɵɵelementStart(14, "a", 13);
        ɵɵelement(15, "i", 14);
        ɵɵelementEnd();
        ɵɵelementStart(16, "a", 15);
        ɵɵelement(17, "i", 16);
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(18, "div", 17);
        ɵɵelementStart(19, "div", 18);
        ɵɵelementStart(20, "div", 19);
        ɵɵelementStart(21, "h6", 20);
        ɵɵtext(22, "Company name");
        ɵɵelementEnd();
        ɵɵelement(23, "hr", 21);
        ɵɵelementStart(24, "p");
        ɵɵtext(25, " Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet, consectetur adipisicing elit. ");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(26, "div", 22);
        ɵɵelementStart(27, "h6", 20);
        ɵɵtext(28, "Products");
        ɵɵelementEnd();
        ɵɵelement(29, "hr", 21);
        ɵɵelementStart(30, "p");
        ɵɵelementStart(31, "a", 23);
        ɵɵtext(32, "MDBootstrap");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(33, "p");
        ɵɵelementStart(34, "a", 23);
        ɵɵtext(35, "MDWordPress");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(36, "p");
        ɵɵelementStart(37, "a", 23);
        ɵɵtext(38, "BrandFlow");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(39, "p");
        ɵɵelementStart(40, "a", 23);
        ɵɵtext(41, "Bootstrap Angular");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(42, "div", 24);
        ɵɵelementStart(43, "h6", 20);
        ɵɵtext(44, "Useful links");
        ɵɵelementEnd();
        ɵɵelement(45, "hr", 21);
        ɵɵelementStart(46, "p");
        ɵɵelementStart(47, "a", 23);
        ɵɵtext(48, "Your Account");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(49, "p");
        ɵɵelementStart(50, "a", 23);
        ɵɵtext(51, "Become an Affiliate");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(52, "p");
        ɵɵelementStart(53, "a", 23);
        ɵɵtext(54, "Shipping Rates");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(55, "p");
        ɵɵelementStart(56, "a", 23);
        ɵɵtext(57, "Help");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(58, "div", 25);
        ɵɵelementStart(59, "h6", 20);
        ɵɵtext(60, "Contact");
        ɵɵelementEnd();
        ɵɵelement(61, "hr", 21);
        ɵɵelementStart(62, "p");
        ɵɵelement(63, "i", 26);
        ɵɵtext(64, " New York, NY 10012, US");
        ɵɵelementEnd();
        ɵɵelementStart(65, "p");
        ɵɵelement(66, "i", 27);
        ɵɵtext(67, " info@example.com");
        ɵɵelementEnd();
        ɵɵelementStart(68, "p");
        ɵɵelement(69, "i", 28);
        ɵɵtext(70, " + 01 234 567 88");
        ɵɵelementEnd();
        ɵɵelementStart(71, "p");
        ɵɵelement(72, "i", 29);
        ɵɵtext(73, " + 01 234 567 89");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(74, "div", 30);
        ɵɵtext(75, " \u00A9 2020 Copyright: ");
        ɵɵelementEnd();
        ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(FooterComponent, [{
        type: Component,
        args: [{
                selector: 'lib-footer',
                templateUrl: './footer.component.html',
                styleUrls: ['./footer.component.css']
            }]
    }], function () { return []; }, null); })();

class FooterModule {
}
FooterModule.ɵmod = ɵɵdefineNgModule({ type: FooterModule });
FooterModule.ɵinj = ɵɵdefineInjector({ factory: function FooterModule_Factory(t) { return new (t || FooterModule)(); }, providers: [], imports: [[CommonModule, HttpClientModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(FooterModule, { declarations: [FooterComponent], imports: [CommonModule, HttpClientModule], exports: [FooterComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(FooterModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule, HttpClientModule],
                declarations: [FooterComponent],
                providers: [],
                exports: [FooterComponent],
            }]
    }], null, null); })();

class NavbarComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
NavbarComponent.ɵfac = function NavbarComponent_Factory(t) { return new (t || NavbarComponent)(); };
NavbarComponent.ɵcmp = ɵɵdefineComponent({ type: NavbarComponent, selectors: [["lib-navbar"]], decls: 22, vars: 0, consts: [[1, "navbar", "navbar-expand-lg", "navbar-dark", "bg-dark"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarTogglerDemo03", "aria-controls", "navbarTogglerDemo03", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler"], [1, "navbar-toggler-icon"], ["href", "#", 1, "navbar-brand"], ["id", "navbarTogglerDemo03", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "mr-auto", "mt-2", "mt-lg-0"], [1, "nav-item"], ["href", "#", 1, "nav-link"], [1, "sr-only"], [1, "form-inline", "my-2", "my-lg-0"], ["type", "search", "placeholder", "Search", "aria-label", "Search", 1, "form-control", "mr-sm-2"], ["type", "submit", 1, "btn", "btn-outline-primary", "my-2", "my-sm-0"]], template: function NavbarComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "nav", 0);
        ɵɵelementStart(1, "button", 1);
        ɵɵelement(2, "span", 2);
        ɵɵelementEnd();
        ɵɵelementStart(3, "a", 3);
        ɵɵtext(4, "Brand");
        ɵɵelementEnd();
        ɵɵelementStart(5, "div", 4);
        ɵɵelementStart(6, "ul", 5);
        ɵɵelementStart(7, "li", 6);
        ɵɵelementStart(8, "a", 7);
        ɵɵtext(9, "Home ");
        ɵɵelementStart(10, "span", 8);
        ɵɵtext(11, "(current)");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(12, "li", 6);
        ɵɵelementStart(13, "a", 7);
        ɵɵtext(14, "Link");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(15, "li", 6);
        ɵɵelementStart(16, "a", 7);
        ɵɵtext(17, "About");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(18, "form", 9);
        ɵɵelement(19, "input", 10);
        ɵɵelementStart(20, "button", 11);
        ɵɵtext(21, " Search ");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(NavbarComponent, [{
        type: Component,
        args: [{
                selector: 'lib-navbar',
                templateUrl: './navbar.component.html',
                styleUrls: ['./navbar.component.css']
            }]
    }], function () { return []; }, null); })();

class NavbarModule {
}
NavbarModule.ɵmod = ɵɵdefineNgModule({ type: NavbarModule });
NavbarModule.ɵinj = ɵɵdefineInjector({ factory: function NavbarModule_Factory(t) { return new (t || NavbarModule)(); }, providers: [], imports: [[CommonModule, HttpClientModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(NavbarModule, { declarations: [NavbarComponent], imports: [CommonModule, HttpClientModule], exports: [NavbarComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(NavbarModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule, HttpClientModule],
                declarations: [NavbarComponent],
                providers: [],
                exports: [NavbarComponent],
            }]
    }], null, null); })();

/*
 * Public API Surface of common-web-component
 */

/**
 * Generated bundle index. Do not edit.
 */

export { ButtonComponent, ButtonModule, FooterComponent, FooterModule, HelloComponent, HelloModule, NavbarComponent, NavbarModule };
//# sourceMappingURL=common-web-component.js.map
