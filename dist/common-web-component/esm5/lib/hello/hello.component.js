import { Component } from '@angular/core';
import * as i0 from "@angular/core";
var HelloComponent = /** @class */ (function () {
    function HelloComponent() {
    }
    HelloComponent.prototype.ngOnInit = function () {
    };
    HelloComponent.ɵfac = function HelloComponent_Factory(t) { return new (t || HelloComponent)(); };
    HelloComponent.ɵcmp = i0.ɵɵdefineComponent({ type: HelloComponent, selectors: [["lib-hello"]], decls: 2, vars: 0, template: function HelloComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "p");
            i0.ɵɵtext(1, "This is from hello component");
            i0.ɵɵelementEnd();
        } }, styles: [""] });
    return HelloComponent;
}());
export { HelloComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(HelloComponent, [{
        type: Component,
        args: [{
                selector: 'lib-hello',
                templateUrl: './hello.component.html',
                styleUrls: ['./hello.component.css']
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVsbG8uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXdlYi1jb21wb25lbnQvIiwic291cmNlcyI6WyJsaWIvaGVsbG8vaGVsbG8uY29tcG9uZW50LnRzIiwibGliL2hlbGxvL2hlbGxvLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7O0FBRWxEO0lBT0U7SUFBZ0IsQ0FBQztJQUVqQixpQ0FBUSxHQUFSO0lBQ0EsQ0FBQztnRkFMVSxjQUFjO3VEQUFkLGNBQWM7WUNQM0IseUJBQUc7WUFBQSw0Q0FBNEI7WUFBQSxpQkFBSTs7eUJEQW5DO0NBY0MsQUFaRCxJQVlDO1NBUFksY0FBYztrREFBZCxjQUFjO2NBTDFCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsV0FBVztnQkFDckIsV0FBVyxFQUFFLHdCQUF3QjtnQkFDckMsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7YUFDckMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItaGVsbG8nLFxuICB0ZW1wbGF0ZVVybDogJy4vaGVsbG8uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9oZWxsby5jb21wb25lbnQuY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgSGVsbG9Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gIH1cblxufVxuIiwiPHA+VGhpcyBpcyBmcm9tIGhlbGxvIGNvbXBvbmVudDwvcD5cbiJdfQ==