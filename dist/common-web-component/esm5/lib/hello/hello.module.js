import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HelloComponent } from "./hello.component";
import { HttpClientModule } from "@angular/common/http";
import * as i0 from "@angular/core";
var HelloModule = /** @class */ (function () {
    function HelloModule() {
    }
    HelloModule.ɵmod = i0.ɵɵdefineNgModule({ type: HelloModule });
    HelloModule.ɵinj = i0.ɵɵdefineInjector({ factory: function HelloModule_Factory(t) { return new (t || HelloModule)(); }, providers: [], imports: [[CommonModule, HttpClientModule]] });
    return HelloModule;
}());
export { HelloModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(HelloModule, { declarations: [HelloComponent], imports: [CommonModule, HttpClientModule], exports: [HelloComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(HelloModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule, HttpClientModule],
                declarations: [HelloComponent],
                providers: [],
                exports: [HelloComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVsbG8ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXdlYi1jb21wb25lbnQvIiwic291cmNlcyI6WyJsaWIvaGVsbG8vaGVsbG8ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUVuRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7QUFFeEQ7SUFBQTtLQU0yQjttREFBZCxXQUFXO3lHQUFYLFdBQVcsbUJBSFgsRUFBRSxZQUZKLENBQUMsWUFBWSxFQUFFLGdCQUFnQixDQUFDO3NCQVIzQztDQWEyQixBQU4zQixJQU0yQjtTQUFkLFdBQVc7d0ZBQVgsV0FBVyxtQkFKUCxjQUFjLGFBRG5CLFlBQVksRUFBRSxnQkFBZ0IsYUFHOUIsY0FBYztrREFFYixXQUFXO2NBTnZCLFFBQVE7ZUFBQztnQkFDUixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsZ0JBQWdCLENBQUM7Z0JBQ3pDLFlBQVksRUFBRSxDQUFDLGNBQWMsQ0FBQztnQkFDOUIsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsT0FBTyxFQUFFLENBQUMsY0FBYyxDQUFDO2FBQzFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XHJcblxyXG5pbXBvcnQgeyBIZWxsb0NvbXBvbmVudCB9IGZyb20gXCIuL2hlbGxvLmNvbXBvbmVudFwiO1xyXG5cclxuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlLCBIdHRwQ2xpZW50TW9kdWxlXSxcclxuICBkZWNsYXJhdGlvbnM6IFtIZWxsb0NvbXBvbmVudF0sXHJcbiAgcHJvdmlkZXJzOiBbXSxcclxuICBleHBvcnRzOiBbSGVsbG9Db21wb25lbnRdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgSGVsbG9Nb2R1bGUge31cclxuIl19