import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FooterComponent } from "./footer.component";
import { HttpClientModule } from "@angular/common/http";
import * as i0 from "@angular/core";
var FooterModule = /** @class */ (function () {
    function FooterModule() {
    }
    FooterModule.ɵmod = i0.ɵɵdefineNgModule({ type: FooterModule });
    FooterModule.ɵinj = i0.ɵɵdefineInjector({ factory: function FooterModule_Factory(t) { return new (t || FooterModule)(); }, providers: [], imports: [[CommonModule, HttpClientModule]] });
    return FooterModule;
}());
export { FooterModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(FooterModule, { declarations: [FooterComponent], imports: [CommonModule, HttpClientModule], exports: [FooterComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FooterModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule, HttpClientModule],
                declarations: [FooterComponent],
                providers: [],
                exports: [FooterComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGVyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi13ZWItY29tcG9uZW50LyIsInNvdXJjZXMiOlsibGliL2Zvb3Rlci9mb290ZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVyRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7QUFFeEQ7SUFBQTtLQU00QjtvREFBZixZQUFZOzJHQUFaLFlBQVksbUJBSFosRUFBRSxZQUZKLENBQUMsWUFBWSxFQUFFLGdCQUFnQixDQUFDO3VCQVIzQztDQWE0QixBQU41QixJQU00QjtTQUFmLFlBQVk7d0ZBQVosWUFBWSxtQkFKUixlQUFlLGFBRHBCLFlBQVksRUFBRSxnQkFBZ0IsYUFHOUIsZUFBZTtrREFFZCxZQUFZO2NBTnhCLFFBQVE7ZUFBQztnQkFDUixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsZ0JBQWdCLENBQUM7Z0JBQ3pDLFlBQVksRUFBRSxDQUFDLGVBQWUsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsT0FBTyxFQUFFLENBQUMsZUFBZSxDQUFDO2FBQzNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XHJcblxyXG5pbXBvcnQgeyBGb290ZXJDb21wb25lbnQgfSBmcm9tIFwiLi9mb290ZXIuY29tcG9uZW50XCI7XHJcblxyXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsIEh0dHBDbGllbnRNb2R1bGVdLFxyXG4gIGRlY2xhcmF0aW9uczogW0Zvb3RlckNvbXBvbmVudF0sXHJcbiAgcHJvdmlkZXJzOiBbXSxcclxuICBleHBvcnRzOiBbRm9vdGVyQ29tcG9uZW50XSxcclxufSlcclxuZXhwb3J0IGNsYXNzIEZvb3Rlck1vZHVsZSB7fVxyXG4iXX0=