import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NavbarComponent } from "./navbar.component";
import { HttpClientModule } from "@angular/common/http";
import * as i0 from "@angular/core";
var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule.ɵmod = i0.ɵɵdefineNgModule({ type: NavbarModule });
    NavbarModule.ɵinj = i0.ɵɵdefineInjector({ factory: function NavbarModule_Factory(t) { return new (t || NavbarModule)(); }, providers: [], imports: [[CommonModule, HttpClientModule]] });
    return NavbarModule;
}());
export { NavbarModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(NavbarModule, { declarations: [NavbarComponent], imports: [CommonModule, HttpClientModule], exports: [NavbarComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(NavbarModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule, HttpClientModule],
                declarations: [NavbarComponent],
                providers: [],
                exports: [NavbarComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2YmFyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi13ZWItY29tcG9uZW50LyIsInNvdXJjZXMiOlsibGliL25hdmJhci9uYXZiYXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVyRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7QUFFeEQ7SUFBQTtLQU00QjtvREFBZixZQUFZOzJHQUFaLFlBQVksbUJBSFosRUFBRSxZQUZKLENBQUMsWUFBWSxFQUFFLGdCQUFnQixDQUFDO3VCQVIzQztDQWE0QixBQU41QixJQU00QjtTQUFmLFlBQVk7d0ZBQVosWUFBWSxtQkFKUixlQUFlLGFBRHBCLFlBQVksRUFBRSxnQkFBZ0IsYUFHOUIsZUFBZTtrREFFZCxZQUFZO2NBTnhCLFFBQVE7ZUFBQztnQkFDUixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsZ0JBQWdCLENBQUM7Z0JBQ3pDLFlBQVksRUFBRSxDQUFDLGVBQWUsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsT0FBTyxFQUFFLENBQUMsZUFBZSxDQUFDO2FBQzNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xuXG5pbXBvcnQgeyBOYXZiYXJDb21wb25lbnQgfSBmcm9tIFwiLi9uYXZiYXIuY29tcG9uZW50XCI7XG5cbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgSHR0cENsaWVudE1vZHVsZV0sXG4gIGRlY2xhcmF0aW9uczogW05hdmJhckNvbXBvbmVudF0sXG4gIHByb3ZpZGVyczogW10sXG4gIGV4cG9ydHM6IFtOYXZiYXJDb21wb25lbnRdLFxufSlcbmV4cG9ydCBjbGFzcyBOYXZiYXJNb2R1bGUge31cbiJdfQ==