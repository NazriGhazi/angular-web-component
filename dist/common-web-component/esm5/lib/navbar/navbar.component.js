import { Component } from '@angular/core';
import * as i0 from "@angular/core";
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.ɵfac = function NavbarComponent_Factory(t) { return new (t || NavbarComponent)(); };
    NavbarComponent.ɵcmp = i0.ɵɵdefineComponent({ type: NavbarComponent, selectors: [["lib-navbar"]], decls: 22, vars: 0, consts: [[1, "navbar", "navbar-expand-lg", "navbar-dark", "bg-dark"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarTogglerDemo03", "aria-controls", "navbarTogglerDemo03", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler"], [1, "navbar-toggler-icon"], ["href", "#", 1, "navbar-brand"], ["id", "navbarTogglerDemo03", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "mr-auto", "mt-2", "mt-lg-0"], [1, "nav-item"], ["href", "#", 1, "nav-link"], [1, "sr-only"], [1, "form-inline", "my-2", "my-lg-0"], ["type", "search", "placeholder", "Search", "aria-label", "Search", 1, "form-control", "mr-sm-2"], ["type", "submit", 1, "btn", "btn-outline-primary", "my-2", "my-sm-0"]], template: function NavbarComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "nav", 0);
            i0.ɵɵelementStart(1, "button", 1);
            i0.ɵɵelement(2, "span", 2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "a", 3);
            i0.ɵɵtext(4, "Brand");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(5, "div", 4);
            i0.ɵɵelementStart(6, "ul", 5);
            i0.ɵɵelementStart(7, "li", 6);
            i0.ɵɵelementStart(8, "a", 7);
            i0.ɵɵtext(9, "Home ");
            i0.ɵɵelementStart(10, "span", 8);
            i0.ɵɵtext(11, "(current)");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(12, "li", 6);
            i0.ɵɵelementStart(13, "a", 7);
            i0.ɵɵtext(14, "Link");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(15, "li", 6);
            i0.ɵɵelementStart(16, "a", 7);
            i0.ɵɵtext(17, "About");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(18, "form", 9);
            i0.ɵɵelement(19, "input", 10);
            i0.ɵɵelementStart(20, "button", 11);
            i0.ɵɵtext(21, " Search ");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } }, styles: [""] });
    return NavbarComponent;
}());
export { NavbarComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(NavbarComponent, [{
        type: Component,
        args: [{
                selector: 'lib-navbar',
                templateUrl: './navbar.component.html',
                styleUrls: ['./navbar.component.css']
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2YmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi13ZWItY29tcG9uZW50LyIsInNvdXJjZXMiOlsibGliL25hdmJhci9uYXZiYXIuY29tcG9uZW50LnRzIiwibGliL25hdmJhci9uYXZiYXIuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQzs7QUFFbEQ7SUFPRTtJQUFnQixDQUFDO0lBRWpCLGtDQUFRLEdBQVI7SUFDQSxDQUFDO2tGQUxVLGVBQWU7d0RBQWYsZUFBZTtZQ1A1Qiw4QkFDRTtZQUFBLGlDQVNFO1lBQUEsMEJBQXlDO1lBQzNDLGlCQUFTO1lBQ1QsNEJBQWlDO1lBQUEscUJBQUs7WUFBQSxpQkFBSTtZQUUxQyw4QkFDRTtZQUFBLDZCQUNFO1lBQUEsNkJBQ0U7WUFBQSw0QkFDRztZQUFBLHFCQUFLO1lBQUEsZ0NBQXNCO1lBQUEsMEJBQVM7WUFBQSxpQkFBTztZQUFBLGlCQUM3QztZQUNILGlCQUFLO1lBQ0wsOEJBQ0U7WUFBQSw2QkFBNkI7WUFBQSxxQkFBSTtZQUFBLGlCQUFJO1lBQ3ZDLGlCQUFLO1lBQ0wsOEJBQ0U7WUFBQSw2QkFBNkI7WUFBQSxzQkFBSztZQUFBLGlCQUFJO1lBQ3hDLGlCQUFLO1lBQ1AsaUJBQUs7WUFDTCxnQ0FDRTtZQUFBLDZCQU1BO1lBQUEsbUNBQ0U7WUFBQSx5QkFDRjtZQUFBLGlCQUFTO1lBQ1gsaUJBQU87WUFDVCxpQkFBTTtZQUNSLGlCQUFNOzswQkR4Q047Q0FjQyxBQVpELElBWUM7U0FQWSxlQUFlO2tEQUFmLGVBQWU7Y0FMM0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QixXQUFXLEVBQUUseUJBQXlCO2dCQUN0QyxTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQzthQUN0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi1uYXZiYXInLFxuICB0ZW1wbGF0ZVVybDogJy4vbmF2YmFyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbmF2YmFyLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBOYXZiYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gIH1cblxufVxuIiwiPG5hdiBjbGFzcz1cIm5hdmJhciBuYXZiYXItZXhwYW5kLWxnIG5hdmJhci1kYXJrIGJnLWRhcmtcIj5cbiAgPGJ1dHRvblxuICAgIGNsYXNzPVwibmF2YmFyLXRvZ2dsZXJcIlxuICAgIHR5cGU9XCJidXR0b25cIlxuICAgIGRhdGEtdG9nZ2xlPVwiY29sbGFwc2VcIlxuICAgIGRhdGEtdGFyZ2V0PVwiI25hdmJhclRvZ2dsZXJEZW1vMDNcIlxuICAgIGFyaWEtY29udHJvbHM9XCJuYXZiYXJUb2dnbGVyRGVtbzAzXCJcbiAgICBhcmlhLWV4cGFuZGVkPVwiZmFsc2VcIlxuICAgIGFyaWEtbGFiZWw9XCJUb2dnbGUgbmF2aWdhdGlvblwiXG4gID5cbiAgICA8c3BhbiBjbGFzcz1cIm5hdmJhci10b2dnbGVyLWljb25cIj48L3NwYW4+XG4gIDwvYnV0dG9uPlxuICA8YSBjbGFzcz1cIm5hdmJhci1icmFuZFwiIGhyZWY9XCIjXCI+QnJhbmQ8L2E+XG5cbiAgPGRpdiBjbGFzcz1cImNvbGxhcHNlIG5hdmJhci1jb2xsYXBzZVwiIGlkPVwibmF2YmFyVG9nZ2xlckRlbW8wM1wiPlxuICAgIDx1bCBjbGFzcz1cIm5hdmJhci1uYXYgbXItYXV0byBtdC0yIG10LWxnLTBcIj5cbiAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCI+XG4gICAgICAgIDxhIGNsYXNzPVwibmF2LWxpbmtcIiBocmVmPVwiI1wiXG4gICAgICAgICAgPkhvbWUgPHNwYW4gY2xhc3M9XCJzci1vbmx5XCI+KGN1cnJlbnQpPC9zcGFuPjwvYVxuICAgICAgICA+XG4gICAgICA8L2xpPlxuICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIj5cbiAgICAgICAgPGEgY2xhc3M9XCJuYXYtbGlua1wiIGhyZWY9XCIjXCI+TGluazwvYT5cbiAgICAgIDwvbGk+XG4gICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbVwiPlxuICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rXCIgaHJlZj1cIiNcIj5BYm91dDwvYT5cbiAgICAgIDwvbGk+XG4gICAgPC91bD5cbiAgICA8Zm9ybSBjbGFzcz1cImZvcm0taW5saW5lIG15LTIgbXktbGctMFwiPlxuICAgICAgPGlucHV0XG4gICAgICAgIGNsYXNzPVwiZm9ybS1jb250cm9sIG1yLXNtLTJcIlxuICAgICAgICB0eXBlPVwic2VhcmNoXCJcbiAgICAgICAgcGxhY2Vob2xkZXI9XCJTZWFyY2hcIlxuICAgICAgICBhcmlhLWxhYmVsPVwiU2VhcmNoXCJcbiAgICAgIC8+XG4gICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuIGJ0bi1vdXRsaW5lLXByaW1hcnkgbXktMiBteS1zbS0wXCIgdHlwZT1cInN1Ym1pdFwiPlxuICAgICAgICBTZWFyY2hcbiAgICAgIDwvYnV0dG9uPlxuICAgIDwvZm9ybT5cbiAgPC9kaXY+XG48L25hdj5cbiJdfQ==