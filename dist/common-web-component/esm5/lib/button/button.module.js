import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ButtonComponent } from "./button.component";
import { HttpClientModule } from "@angular/common/http";
import * as i0 from "@angular/core";
var ButtonModule = /** @class */ (function () {
    function ButtonModule() {
    }
    ButtonModule.ɵmod = i0.ɵɵdefineNgModule({ type: ButtonModule });
    ButtonModule.ɵinj = i0.ɵɵdefineInjector({ factory: function ButtonModule_Factory(t) { return new (t || ButtonModule)(); }, providers: [], imports: [[CommonModule, HttpClientModule]] });
    return ButtonModule;
}());
export { ButtonModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ButtonModule, { declarations: [ButtonComponent], imports: [CommonModule, HttpClientModule], exports: [ButtonComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ButtonModule, [{
        type: NgModule,
        args: [{
                imports: [CommonModule, HttpClientModule],
                declarations: [ButtonComponent],
                providers: [],
                exports: [ButtonComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi13ZWItY29tcG9uZW50LyIsInNvdXJjZXMiOlsibGliL2J1dHRvbi9idXR0b24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVyRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7QUFFeEQ7SUFBQTtLQU00QjtvREFBZixZQUFZOzJHQUFaLFlBQVksbUJBSFosRUFBRSxZQUZKLENBQUMsWUFBWSxFQUFFLGdCQUFnQixDQUFDO3VCQVIzQztDQWE0QixBQU41QixJQU00QjtTQUFmLFlBQVk7d0ZBQVosWUFBWSxtQkFKUixlQUFlLGFBRHBCLFlBQVksRUFBRSxnQkFBZ0IsYUFHOUIsZUFBZTtrREFFZCxZQUFZO2NBTnhCLFFBQVE7ZUFBQztnQkFDUixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsZ0JBQWdCLENBQUM7Z0JBQ3pDLFlBQVksRUFBRSxDQUFDLGVBQWUsQ0FBQztnQkFDL0IsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsT0FBTyxFQUFFLENBQUMsZUFBZSxDQUFDO2FBQzNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XHJcblxyXG5pbXBvcnQgeyBCdXR0b25Db21wb25lbnQgfSBmcm9tIFwiLi9idXR0b24uY29tcG9uZW50XCI7XHJcblxyXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsIEh0dHBDbGllbnRNb2R1bGVdLFxyXG4gIGRlY2xhcmF0aW9uczogW0J1dHRvbkNvbXBvbmVudF0sXHJcbiAgcHJvdmlkZXJzOiBbXSxcclxuICBleHBvcnRzOiBbQnV0dG9uQ29tcG9uZW50XSxcclxufSlcclxuZXhwb3J0IGNsYXNzIEJ1dHRvbk1vZHVsZSB7fVxyXG4iXX0=