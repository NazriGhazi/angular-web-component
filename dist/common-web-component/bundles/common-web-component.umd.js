(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('@angular/common/http')) :
    typeof define === 'function' && define.amd ? define('common-web-component', ['exports', '@angular/core', '@angular/common', '@angular/common/http'], factory) :
    (global = global || self, factory(global['common-web-component'] = {}, global.ng.core, global.ng.common, global.ng.common.http));
}(this, (function (exports, core, common, http) { 'use strict';

    var ButtonComponent = /** @class */ (function () {
        function ButtonComponent() {
        }
        ButtonComponent.prototype.ngOnInit = function () {
        };
        ButtonComponent.ɵfac = function ButtonComponent_Factory(t) { return new (t || ButtonComponent)(); };
        ButtonComponent.ɵcmp = core.ɵɵdefineComponent({ type: ButtonComponent, selectors: [["lib-button"]], decls: 2, vars: 0, template: function ButtonComponent_Template(rf, ctx) { if (rf & 1) {
                core.ɵɵelementStart(0, "button");
                core.ɵɵtext(1, "Button Works");
                core.ɵɵelementEnd();
            } }, styles: [""] });
        return ButtonComponent;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(ButtonComponent, [{
            type: core.Component,
            args: [{
                    selector: 'lib-button',
                    templateUrl: './button.component.html',
                    styleUrls: ['./button.component.css']
                }]
        }], function () { return []; }, null); })();

    var ButtonModule = /** @class */ (function () {
        function ButtonModule() {
        }
        ButtonModule.ɵmod = core.ɵɵdefineNgModule({ type: ButtonModule });
        ButtonModule.ɵinj = core.ɵɵdefineInjector({ factory: function ButtonModule_Factory(t) { return new (t || ButtonModule)(); }, providers: [], imports: [[common.CommonModule, http.HttpClientModule]] });
        return ButtonModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core.ɵɵsetNgModuleScope(ButtonModule, { declarations: [ButtonComponent], imports: [common.CommonModule, http.HttpClientModule], exports: [ButtonComponent] }); })();
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(ButtonModule, [{
            type: core.NgModule,
            args: [{
                    imports: [common.CommonModule, http.HttpClientModule],
                    declarations: [ButtonComponent],
                    providers: [],
                    exports: [ButtonComponent],
                }]
        }], null, null); })();

    var HelloComponent = /** @class */ (function () {
        function HelloComponent() {
        }
        HelloComponent.prototype.ngOnInit = function () {
        };
        HelloComponent.ɵfac = function HelloComponent_Factory(t) { return new (t || HelloComponent)(); };
        HelloComponent.ɵcmp = core.ɵɵdefineComponent({ type: HelloComponent, selectors: [["lib-hello"]], decls: 2, vars: 0, template: function HelloComponent_Template(rf, ctx) { if (rf & 1) {
                core.ɵɵelementStart(0, "p");
                core.ɵɵtext(1, "This is from hello component");
                core.ɵɵelementEnd();
            } }, styles: [""] });
        return HelloComponent;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(HelloComponent, [{
            type: core.Component,
            args: [{
                    selector: 'lib-hello',
                    templateUrl: './hello.component.html',
                    styleUrls: ['./hello.component.css']
                }]
        }], function () { return []; }, null); })();

    var HelloModule = /** @class */ (function () {
        function HelloModule() {
        }
        HelloModule.ɵmod = core.ɵɵdefineNgModule({ type: HelloModule });
        HelloModule.ɵinj = core.ɵɵdefineInjector({ factory: function HelloModule_Factory(t) { return new (t || HelloModule)(); }, providers: [], imports: [[common.CommonModule, http.HttpClientModule]] });
        return HelloModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core.ɵɵsetNgModuleScope(HelloModule, { declarations: [HelloComponent], imports: [common.CommonModule, http.HttpClientModule], exports: [HelloComponent] }); })();
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(HelloModule, [{
            type: core.NgModule,
            args: [{
                    imports: [common.CommonModule, http.HttpClientModule],
                    declarations: [HelloComponent],
                    providers: [],
                    exports: [HelloComponent],
                }]
        }], null, null); })();

    var FooterComponent = /** @class */ (function () {
        function FooterComponent() {
        }
        FooterComponent.prototype.ngOnInit = function () {
        };
        FooterComponent.ɵfac = function FooterComponent_Factory(t) { return new (t || FooterComponent)(); };
        FooterComponent.ɵcmp = core.ɵɵdefineComponent({ type: FooterComponent, selectors: [["lib-footer"]], decls: 76, vars: 0, consts: [[1, "page-footer", "font-small", "bg-dark", "text-white"], [2, "background-color", "#6351ce"], [1, "container"], [1, "row", "py-4", "d-flex", "align-items-center"], [1, "col-md-6", "col-lg-5", "text-center", "text-md-left", "mb-4", "mb-md-0"], [1, "mb-0"], [1, "col-md-6", "col-lg-7", "text-center", "text-md-right"], [1, "fb-ic"], [1, "fab", "fa-facebook-f", "white-text", "mr-4"], [1, "tw-ic"], [1, "fab", "fa-twitter", "white-text", "mr-4"], [1, "gplus-ic"], [1, "fab", "fa-google-plus-g", "white-text", "mr-4"], [1, "li-ic"], [1, "fab", "fa-linkedin-in", "white-text", "mr-4"], [1, "ins-ic"], [1, "fab", "fa-instagram", "white-text"], [1, "container", "text-center", "text-md-left", "mt-5"], [1, "row", "mt-3"], [1, "col-md-3", "col-lg-4", "col-xl-3", "mx-auto", "mb-4"], [1, "text-uppercase", "font-weight-bold"], [1, "deep-purple", "accent-2", "mb-4", "mt-0", "d-inline-block", "mx-auto", 2, "width", "60px"], [1, "col-md-2", "col-lg-2", "col-xl-2", "mx-auto", "mb-4"], ["href", "#!"], [1, "col-md-3", "col-lg-2", "col-xl-2", "mx-auto", "mb-4"], [1, "col-md-4", "col-lg-3", "col-xl-3", "mx-auto", "mb-md-0", "mb-4"], [1, "fas", "fa-home", "mr-3"], [1, "fas", "fa-envelope", "mr-3"], [1, "fas", "fa-phone", "mr-3"], [1, "fas", "fa-print", "mr-3"], [1, "footer-copyright", "text-center", "py-3"]], template: function FooterComponent_Template(rf, ctx) { if (rf & 1) {
                core.ɵɵelementStart(0, "footer", 0);
                core.ɵɵelementStart(1, "div", 1);
                core.ɵɵelementStart(2, "div", 2);
                core.ɵɵelementStart(3, "div", 3);
                core.ɵɵelementStart(4, "div", 4);
                core.ɵɵelementStart(5, "h6", 5);
                core.ɵɵtext(6, "Get connected with us on social networks!");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(7, "div", 6);
                core.ɵɵelementStart(8, "a", 7);
                core.ɵɵelement(9, "i", 8);
                core.ɵɵelementEnd();
                core.ɵɵelementStart(10, "a", 9);
                core.ɵɵelement(11, "i", 10);
                core.ɵɵelementEnd();
                core.ɵɵelementStart(12, "a", 11);
                core.ɵɵelement(13, "i", 12);
                core.ɵɵelementEnd();
                core.ɵɵelementStart(14, "a", 13);
                core.ɵɵelement(15, "i", 14);
                core.ɵɵelementEnd();
                core.ɵɵelementStart(16, "a", 15);
                core.ɵɵelement(17, "i", 16);
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(18, "div", 17);
                core.ɵɵelementStart(19, "div", 18);
                core.ɵɵelementStart(20, "div", 19);
                core.ɵɵelementStart(21, "h6", 20);
                core.ɵɵtext(22, "Company name");
                core.ɵɵelementEnd();
                core.ɵɵelement(23, "hr", 21);
                core.ɵɵelementStart(24, "p");
                core.ɵɵtext(25, " Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet, consectetur adipisicing elit. ");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(26, "div", 22);
                core.ɵɵelementStart(27, "h6", 20);
                core.ɵɵtext(28, "Products");
                core.ɵɵelementEnd();
                core.ɵɵelement(29, "hr", 21);
                core.ɵɵelementStart(30, "p");
                core.ɵɵelementStart(31, "a", 23);
                core.ɵɵtext(32, "MDBootstrap");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(33, "p");
                core.ɵɵelementStart(34, "a", 23);
                core.ɵɵtext(35, "MDWordPress");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(36, "p");
                core.ɵɵelementStart(37, "a", 23);
                core.ɵɵtext(38, "BrandFlow");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(39, "p");
                core.ɵɵelementStart(40, "a", 23);
                core.ɵɵtext(41, "Bootstrap Angular");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(42, "div", 24);
                core.ɵɵelementStart(43, "h6", 20);
                core.ɵɵtext(44, "Useful links");
                core.ɵɵelementEnd();
                core.ɵɵelement(45, "hr", 21);
                core.ɵɵelementStart(46, "p");
                core.ɵɵelementStart(47, "a", 23);
                core.ɵɵtext(48, "Your Account");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(49, "p");
                core.ɵɵelementStart(50, "a", 23);
                core.ɵɵtext(51, "Become an Affiliate");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(52, "p");
                core.ɵɵelementStart(53, "a", 23);
                core.ɵɵtext(54, "Shipping Rates");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(55, "p");
                core.ɵɵelementStart(56, "a", 23);
                core.ɵɵtext(57, "Help");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(58, "div", 25);
                core.ɵɵelementStart(59, "h6", 20);
                core.ɵɵtext(60, "Contact");
                core.ɵɵelementEnd();
                core.ɵɵelement(61, "hr", 21);
                core.ɵɵelementStart(62, "p");
                core.ɵɵelement(63, "i", 26);
                core.ɵɵtext(64, " New York, NY 10012, US");
                core.ɵɵelementEnd();
                core.ɵɵelementStart(65, "p");
                core.ɵɵelement(66, "i", 27);
                core.ɵɵtext(67, " info@example.com");
                core.ɵɵelementEnd();
                core.ɵɵelementStart(68, "p");
                core.ɵɵelement(69, "i", 28);
                core.ɵɵtext(70, " + 01 234 567 88");
                core.ɵɵelementEnd();
                core.ɵɵelementStart(71, "p");
                core.ɵɵelement(72, "i", 29);
                core.ɵɵtext(73, " + 01 234 567 89");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(74, "div", 30);
                core.ɵɵtext(75, " \u00A9 2020 Copyright: ");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
            } }, styles: [""] });
        return FooterComponent;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(FooterComponent, [{
            type: core.Component,
            args: [{
                    selector: 'lib-footer',
                    templateUrl: './footer.component.html',
                    styleUrls: ['./footer.component.css']
                }]
        }], function () { return []; }, null); })();

    var FooterModule = /** @class */ (function () {
        function FooterModule() {
        }
        FooterModule.ɵmod = core.ɵɵdefineNgModule({ type: FooterModule });
        FooterModule.ɵinj = core.ɵɵdefineInjector({ factory: function FooterModule_Factory(t) { return new (t || FooterModule)(); }, providers: [], imports: [[common.CommonModule, http.HttpClientModule]] });
        return FooterModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core.ɵɵsetNgModuleScope(FooterModule, { declarations: [FooterComponent], imports: [common.CommonModule, http.HttpClientModule], exports: [FooterComponent] }); })();
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(FooterModule, [{
            type: core.NgModule,
            args: [{
                    imports: [common.CommonModule, http.HttpClientModule],
                    declarations: [FooterComponent],
                    providers: [],
                    exports: [FooterComponent],
                }]
        }], null, null); })();

    var NavbarComponent = /** @class */ (function () {
        function NavbarComponent() {
        }
        NavbarComponent.prototype.ngOnInit = function () {
        };
        NavbarComponent.ɵfac = function NavbarComponent_Factory(t) { return new (t || NavbarComponent)(); };
        NavbarComponent.ɵcmp = core.ɵɵdefineComponent({ type: NavbarComponent, selectors: [["lib-navbar"]], decls: 22, vars: 0, consts: [[1, "navbar", "navbar-expand-lg", "navbar-dark", "bg-dark"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarTogglerDemo03", "aria-controls", "navbarTogglerDemo03", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler"], [1, "navbar-toggler-icon"], ["href", "#", 1, "navbar-brand"], ["id", "navbarTogglerDemo03", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "mr-auto", "mt-2", "mt-lg-0"], [1, "nav-item"], ["href", "#", 1, "nav-link"], [1, "sr-only"], [1, "form-inline", "my-2", "my-lg-0"], ["type", "search", "placeholder", "Search", "aria-label", "Search", 1, "form-control", "mr-sm-2"], ["type", "submit", 1, "btn", "btn-outline-primary", "my-2", "my-sm-0"]], template: function NavbarComponent_Template(rf, ctx) { if (rf & 1) {
                core.ɵɵelementStart(0, "nav", 0);
                core.ɵɵelementStart(1, "button", 1);
                core.ɵɵelement(2, "span", 2);
                core.ɵɵelementEnd();
                core.ɵɵelementStart(3, "a", 3);
                core.ɵɵtext(4, "Brand");
                core.ɵɵelementEnd();
                core.ɵɵelementStart(5, "div", 4);
                core.ɵɵelementStart(6, "ul", 5);
                core.ɵɵelementStart(7, "li", 6);
                core.ɵɵelementStart(8, "a", 7);
                core.ɵɵtext(9, "Home ");
                core.ɵɵelementStart(10, "span", 8);
                core.ɵɵtext(11, "(current)");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(12, "li", 6);
                core.ɵɵelementStart(13, "a", 7);
                core.ɵɵtext(14, "Link");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(15, "li", 6);
                core.ɵɵelementStart(16, "a", 7);
                core.ɵɵtext(17, "About");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementStart(18, "form", 9);
                core.ɵɵelement(19, "input", 10);
                core.ɵɵelementStart(20, "button", 11);
                core.ɵɵtext(21, " Search ");
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
            } }, styles: [""] });
        return NavbarComponent;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(NavbarComponent, [{
            type: core.Component,
            args: [{
                    selector: 'lib-navbar',
                    templateUrl: './navbar.component.html',
                    styleUrls: ['./navbar.component.css']
                }]
        }], function () { return []; }, null); })();

    var NavbarModule = /** @class */ (function () {
        function NavbarModule() {
        }
        NavbarModule.ɵmod = core.ɵɵdefineNgModule({ type: NavbarModule });
        NavbarModule.ɵinj = core.ɵɵdefineInjector({ factory: function NavbarModule_Factory(t) { return new (t || NavbarModule)(); }, providers: [], imports: [[common.CommonModule, http.HttpClientModule]] });
        return NavbarModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core.ɵɵsetNgModuleScope(NavbarModule, { declarations: [NavbarComponent], imports: [common.CommonModule, http.HttpClientModule], exports: [NavbarComponent] }); })();
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(NavbarModule, [{
            type: core.NgModule,
            args: [{
                    imports: [common.CommonModule, http.HttpClientModule],
                    declarations: [NavbarComponent],
                    providers: [],
                    exports: [NavbarComponent],
                }]
        }], null, null); })();

    exports.ButtonComponent = ButtonComponent;
    exports.ButtonModule = ButtonModule;
    exports.FooterComponent = FooterComponent;
    exports.FooterModule = FooterModule;
    exports.HelloComponent = HelloComponent;
    exports.HelloModule = HelloModule;
    exports.NavbarComponent = NavbarComponent;
    exports.NavbarModule = NavbarModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=common-web-component.umd.js.map
