## First time clone

run [ npm i ] to install node_modules

## Library and application location

common-web-component is angular library that contain components (header,navbar,footer) and common-web-element is the application

## Generate component

Generate component in library (common-web-component/src/lib) and update the public-api.ts

## Add custom element

Update app.module.ts in the application (common-web-element/src/app) by adding the new component that you have generated before

## generate bundle

run [npm run build:prod:bundle] to generate bundle js in dist folder

## For More Info

create web component - https://indepth.dev/angular-web-components-a-complete-guide/

bundle js file - https://stackoverflow.com/questions/56420815/how-to-build-bundle-js-with-angular-8

## IMPORTANT NOTE!!

Please install bash (https://altis.com.au/installing-ubuntu-bash-for-windows-10/)
\*run bash in cmd/powershell to generate bundle (concat only recognized by bash).

Install nodejs and npm in bash using nvm (https://phoenixnap.com/kb/update-node-js-version)
